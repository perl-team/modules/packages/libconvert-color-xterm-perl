Source: libconvert-color-xterm-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Andrej Shadura <andrewsh@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libmodule-build-perl
Build-Depends-Indep: libconvert-color-perl <!nocheck>,
                     perl
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libconvert-color-xterm-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libconvert-color-xterm-perl.git
Homepage: https://metacpan.org/release/Convert-Color-XTerm
Rules-Requires-Root: no

Package: libconvert-color-xterm-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libconvert-color-perl
Multi-Arch: foreign
Description: module for indexed colors used by XTerm
 Convert::Color::XTerm is subclass of Convert::Color::RGB8 and provides lookup
 of the colors that xterm uses by default. Note that the module is not
 intelligent enough to actually parse the XTerm configuration on a machine,
 nor to query a running terminal for its actual colors. It simply implements
 the colors that are present as defaults in the XTerm source code.
 .
 It implements the complete 256-color model in XTerm.
